(defproject urix/spa_framework "0.0.1"
  :aot :all
  :source-paths ["src"]
  :resource-paths ["resources"]
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [org.clojure/clojurescript "1.9.946"]
                 [org.clojure/core.async "0.3.465" :scope "test"]
                 [ring/ring-core "1.6.3"]
                 [compojure "1.6.0"]
                 [rm-hull/ring-gzip-middleware "0.1.7"]
                 [http-kit "2.2.0"]
                 [http-kit-sockjs "0.2.0"]
                 [com.cognitect/transit-clj "0.8.309"]
                 [com.cognitect/transit-cljs "0.8.256"]

                 [clj-time "0.14.3"]
                 [clj-http "3.8.0"]
                 [clj-json "0.5.3"]

                 [org.clojure/java.jdbc "0.6.1"]

                 [migratus "1.0.6"]])
