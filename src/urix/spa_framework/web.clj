(ns urix.spa_framework.web
  (:gen-class)
  (:require [ring.middleware.params :refer [wrap-params]]
            [ring.middleware.keyword-params :refer [wrap-keyword-params]]
            [ring.middleware.session :refer [wrap-session]]
            [ring.middleware.cookies :refer [wrap-cookies]]
            [ring.middleware.not-modified :refer [wrap-not-modified]]
            [ring.middleware.resource :refer [wrap-resource]]
            [ring.middleware.gzip :refer [wrap-gzip]]
            [ring.middleware.content-type :refer [wrap-content-type]]
            [ring.util.response :refer [content-type response]]
            [clojure.walk :refer [keywordize-keys]]
            compojure.route
            compojure.core
            [clojure.java.jdbc :as jdbc]
            [migratus.core :as migratus]
            [clj-json.core :as json]
            [methojure.sockjs.session :as sj-sess]
            [methojure.sockjs.core :refer [sockjs-handler]]
            [cognitect.transit :as transit]
            org.httpkit.server
            [urix.spa_framework.oauth2 :refer [wrap-oauth2-vk make-session]]))


(declare web-app)

(defn http-server-start [rpc-function {:as config port :port oauth2 :oauth2 dsn :dsn}]
  (org.httpkit.server/run-server (web-app rpc-function config) {:port port}))

(defn http-server-stop [instance] (instance))

(defn read-transit [s]
  (-> s
      .getBytes
      java.io.ByteArrayInputStream.
      (transit/reader :json)
      transit/read))

(defn write-transit [d]
  (let [out (java.io.ByteArrayOutputStream.)
        writer (transit/writer out :json)]
    (transit/write writer d)
    (.toString out)))

(extend-protocol jdbc/IResultSetReadColumn
  java.sql.Clob
  (result-set-read-column [obj metadata idx]
    (slurp (.getCharacterStream obj))))

(defn send! [sj data] (sj-sess/send! sj {:type :msg :content (write-transit data)}))

(defn make-SJC [rpc-function {:as config :keys [dsn]}]
  (reify
    sj-sess/SockjsConnection
    (on-open [this session] session)
    (on-message [this session msg]
      (try
        (let [{:as inp :keys [id]} (read-transit msg)
              output (rpc-function dsn inp)]
          (send! session (assoc output :id id)))
        session
        (catch Exception e)))
    (on-close [this session] session)))

(defn web-app [rpc-function {dsn :dsn oauth2 :oauth2 :as config}]
  (let [index-html (clojure.java.io/resource "public/index.html")]
    (-> (compojure.core/routes
         (sockjs-handler "/sockjs" (make-SJC rpc-function config) {:response-limit 4096})
         (compojure.core/GET "/" [] index-html)
         (compojure.core/GET "/:name.html" [name] index-html)
         (compojure.core/GET "/oauth2/vk/test" req (-> req str response (content-type "text/plain")))
         (compojure.route/not-found "Not Found"))
        (wrap-resource "public" {:allow-symlinks? true})
        (wrap-oauth2-vk (-> oauth2 :vk))
        (wrap-session {:cookie-name "sid"
                       :store (make-session dsn)
                       :cookie-attrs {:http-only false}})
        wrap-keyword-params
        wrap-params
        wrap-content-type
        wrap-not-modified
        wrap-gzip)))

(defn migrate [{dsn :dsn}]
  (migratus.core/migrate {:store                :database
                          :migration-dir        "migrations"
                          :init-script          "init.sql"
                          :migration-table-name "migration_table"
                          :db (dsn)}))



