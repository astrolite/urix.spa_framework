(ns urix.spa_framework.client
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [cognitect.transit :as transit]
            [cljs.core.async :as ca :refer [<!]]
            cljs.pprint
            goog.net.Cookies
            sockjs))

(defn deb [& x] (js/console.log (with-out-str (cljs.pprint/pprint x))))

(defonce sj (atom nil))

(defonce callbacks (atom {}))

(defonce id-counter (atom 0))

(defn get-sid [] (.get (goog.net.Cookies. js/document) "sid"))

(defn del-sid [] (.remove (goog.net.Cookies. js/document) "sid"))

(defn rpc [method & params]
  (let [ch (ca/chan 1)
        w (transit/writer :json)
        id @id-counter]
    (swap! callbacks assoc id ch)
    (swap! id-counter inc)
    (.send @sj (transit/write w {:id id :method (name method) :params (or params [])}))
    ch))

(defn init-rpc [& {:keys [onopen url] :or {url "/sockjs"}}]
  (reset! sj (js/SockJS. url))
  (js/goog.object.set @sj "onopen"
                      (fn []
                        (js/goog.object.set
                         @sj
                         "onmessage"
                         #(let [data (transit/read (transit/reader :json) (-> % .-data))
                                id (:id data)
                                ch (get @callbacks id)]
                            (ca/put! ch data)
                            (swap! callbacks dissoc id)
                            ))
                        (onopen))))

