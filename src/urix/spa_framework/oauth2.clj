(ns urix.spa_framework.oauth2
  (:gen-class)
  (:require [ring.util.codec :refer [form-encode]]
            [ring.util.response :refer [content-type response redirect]]
            ring.middleware.session.store
            compojure.core
            [clojure.walk :refer [keywordize-keys]]
            [clj-http.client :as http]
            [clj-json.core :as json]
            [clj-time.format :as dtf]
            [clj-time.core :refer [now]]
            [clj-json.core :as json]
            [clojure.java.jdbc :as jdbc]))


(defn authorize-uri [client-params csrf-token]
  (str (:authorize-uri client-params)
       "?"
       (form-encode
        {:client_id (:client-id client-params)
         :redirect_uri (:redirect-uri client-params)
         :scope (:scope client-params)
         :response_type "code"
         :state csrf-token})))

(defn get-authentication-response [oauth2-params csrf-token response-params]
  (if (= (str csrf-token) (str (:state response-params)))
    (try
      (let [body
            (-> (http/post (:access-token-uri oauth2-params)
                           {:form-params {:code          (:code response-params)
                                          :v             "5.74"
                                          ;;:grant_type    "client_credentials" #_ "authorization_code"
                                          :client_id     (:client-id oauth2-params)
                                          :client_secret (:client-secret oauth2-params)
                                          :redirect_uri  (:redirect-uri oauth2-params)}
                            ;;:basic-auth [(:client-id oauth2-params) (:client-secret oauth2-params)]
                            ;;:debug       true
                            ;;:debug-body  true
                            :throw-exceptions false
                            :as          :json})
                :body)]
        (if (string? body)
          (json/parse-string body true)
          body))
      (catch Exception _ nil))
    nil))

(defn generate-new-random-key [] (-> (java.util.UUID/randomUUID) .toString))

(extend-protocol jdbc/IResultSetReadColumn
  java.sql.Clob
  (result-set-read-column [obj metadata idx]
    (slurp (.getCharacterStream obj))))

(defn make-session [dsn]
  #_ "Same as (ring.middleware.session.memory/memory-store session-store)"
  (reify
    ring.middleware.session.store/SessionStore

    (read-session [_ id]
      (let [[{:keys [content]}] (jdbc/query (dsn) ["select * from sessions where id=?" id])]
        (if content
          (-> content
              json/parse-string
              keywordize-keys))))

    (write-session [_ id val]
      (let [key (or id (generate-new-random-key))]
        (jdbc/delete! (dsn) :sessions ["id=?" id])
        (jdbc/insert! (dsn) :sessions {:id key :content (json/generate-string val)})
        key))

    (delete-session [_ id]
      (jdbc/delete! (dsn) :sessions ["id=?" id])
      nil)))

(defn get-user-info [access_token]
  (let [{{[{:keys [id first_name last_name] :as info}] :response} :body}
        (http/post (str "https://api.vk.com/method/users.get" "?"
                        (ring.util.codec/form-encode
                         {:v "5.74" :access_token access_token}))
                   {:as :json :throw-exceptions false})]
    info))

(defn wrap-oauth2-vk [routes conf]
  ;; https://leonid.shevtsov.me/post/oauth2-is-easy/
  (-> (compojure.core/routes
       (compojure.core/GET "/oauth2/vk/start" req
                           (let [state (int (* 1000000000 (rand)))]
                             (-> (authorize-uri conf state)
                                 redirect
                                 (assoc :session {:state state}))))
       (compojure.core/GET "/oauth2/vk/callback" req
                           (fn [{:as req
                                 {session-state :state} :session
                                 {params-state :state code :code :as params} :params}]
                             (let [{:as auth-resp :keys [access_token user_id email]}
                                   (get-authentication-response conf session-state params)
                                   user_info (get-user-info access_token)]
                               (if (and access_token user_id email)
                                 (-> conf
                                     :landing-uri
                                     redirect
                                     (assoc :session (merge
                                                      user_info
                                                      {:access_token access_token
                                                       :user_id user_id
                                                       :email email
                                                       :social :vk
                                                       :now (dtf/unparse (dtf/formatter-local "YYYY-MM-dd HH:mm:ss") (now))})))
                                 (-> "Auth error"
                                     response
                                     (content-type "text/plain"))))))
       routes)))
